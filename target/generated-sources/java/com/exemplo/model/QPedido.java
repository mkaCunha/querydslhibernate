package com.exemplo.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPedido is a Querydsl query type for Pedido
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPedido extends EntityPathBase<Pedido> {

    private static final long serialVersionUID = -497720627L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPedido pedido = new QPedido("pedido");

    public final DatePath<java.util.Date> dataPedido = createDate("dataPedido", java.util.Date.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final QPessoa pessoa;

    public final EnumPath<TipoPedido> tipoPedido = createEnum("tipoPedido", TipoPedido.class);

    public final NumberPath<java.math.BigDecimal> vlTotal = createNumber("vlTotal", java.math.BigDecimal.class);

    public QPedido(String variable) {
        this(Pedido.class, forVariable(variable), INITS);
    }

    public QPedido(Path<? extends Pedido> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPedido(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPedido(PathMetadata<?> metadata, PathInits inits) {
        this(Pedido.class, metadata, inits);
    }

    public QPedido(Class<? extends Pedido> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.pessoa = inits.isInitialized("pessoa") ? new QPessoa(forProperty("pessoa"), inits.get("pessoa")) : null;
    }

}

