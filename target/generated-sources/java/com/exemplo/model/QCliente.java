package com.exemplo.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCliente is a Querydsl query type for Cliente
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCliente extends EntityPathBase<Cliente> {

    private static final long serialVersionUID = -992171150L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCliente cliente = new QCliente("cliente");

    public final QPessoa _super;

    // inherited
    public final QCidade cidade;

    //inherited
    public final StringPath dsPessoa;

    //inherited
    public final SetPath<Endereco, QEndereco> enderecos;

    //inherited
    public final NumberPath<Integer> idPessoa;

    public final StringPath nrCpf = createString("nrCpf");

    public final NumberPath<Integer> nrIdade = createNumber("nrIdade", Integer.class);

    //inherited
    public final SetPath<Pedido, QPedido> pedidos;

    public QCliente(String variable) {
        this(Cliente.class, forVariable(variable), INITS);
    }

    public QCliente(Path<? extends Cliente> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCliente(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCliente(PathMetadata<?> metadata, PathInits inits) {
        this(Cliente.class, metadata, inits);
    }

    public QCliente(Class<? extends Cliente> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new QPessoa(type, metadata, inits);
        this.cidade = _super.cidade;
        this.dsPessoa = _super.dsPessoa;
        this.enderecos = _super.enderecos;
        this.idPessoa = _super.idPessoa;
        this.pedidos = _super.pedidos;
    }

}

