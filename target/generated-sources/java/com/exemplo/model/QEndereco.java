package com.exemplo.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QEndereco is a Querydsl query type for Endereco
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QEndereco extends EntityPathBase<Endereco> {

    private static final long serialVersionUID = 130085201L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEndereco endereco = new QEndereco("endereco");

    public final StringPath dsBairro = createString("dsBairro");

    public final StringPath dsLogradouro = createString("dsLogradouro");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath nrLogradouro = createString("nrLogradouro");

    public final QPessoa pessoa;

    public QEndereco(String variable) {
        this(Endereco.class, forVariable(variable), INITS);
    }

    public QEndereco(Path<? extends Endereco> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QEndereco(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QEndereco(PathMetadata<?> metadata, PathInits inits) {
        this(Endereco.class, metadata, inits);
    }

    public QEndereco(Class<? extends Endereco> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.pessoa = inits.isInitialized("pessoa") ? new QPessoa(forProperty("pessoa"), inits.get("pessoa")) : null;
    }

}

