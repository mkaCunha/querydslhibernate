package com.exemplo.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPessoa is a Querydsl query type for Pessoa
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPessoa extends EntityPathBase<Pessoa> {

    private static final long serialVersionUID = -497263825L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPessoa pessoa = new QPessoa("pessoa");

    public final QCidade cidade;

    public final StringPath dsPessoa = createString("dsPessoa");

    public final SetPath<Endereco, QEndereco> enderecos = this.<Endereco, QEndereco>createSet("enderecos", Endereco.class, QEndereco.class, PathInits.DIRECT2);

    public final NumberPath<Integer> idPessoa = createNumber("idPessoa", Integer.class);

    public final SetPath<Pedido, QPedido> pedidos = this.<Pedido, QPedido>createSet("pedidos", Pedido.class, QPedido.class, PathInits.DIRECT2);

    public QPessoa(String variable) {
        this(Pessoa.class, forVariable(variable), INITS);
    }

    public QPessoa(Path<? extends Pessoa> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPessoa(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPessoa(PathMetadata<?> metadata, PathInits inits) {
        this(Pessoa.class, metadata, inits);
    }

    public QPessoa(Class<? extends Pessoa> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.cidade = inits.isInitialized("cidade") ? new QCidade(forProperty("cidade")) : null;
    }

}

