package com.exemplo.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QFornecedor is a Querydsl query type for Fornecedor
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QFornecedor extends EntityPathBase<Fornecedor> {

    private static final long serialVersionUID = 401991917L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QFornecedor fornecedor = new QFornecedor("fornecedor");

    public final QPessoa _super;

    // inherited
    public final QCidade cidade;

    //inherited
    public final StringPath dsPessoa;

    //inherited
    public final SetPath<Endereco, QEndereco> enderecos;

    //inherited
    public final NumberPath<Integer> idPessoa;

    public final StringPath nmFantasia = createString("nmFantasia");

    public final StringPath nrCnpj = createString("nrCnpj");

    //inherited
    public final SetPath<Pedido, QPedido> pedidos;

    public QFornecedor(String variable) {
        this(Fornecedor.class, forVariable(variable), INITS);
    }

    public QFornecedor(Path<? extends Fornecedor> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QFornecedor(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QFornecedor(PathMetadata<?> metadata, PathInits inits) {
        this(Fornecedor.class, metadata, inits);
    }

    public QFornecedor(Class<? extends Fornecedor> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new QPessoa(type, metadata, inits);
        this.cidade = _super.cidade;
        this.dsPessoa = _super.dsPessoa;
        this.enderecos = _super.enderecos;
        this.idPessoa = _super.idPessoa;
        this.pedidos = _super.pedidos;
    }

}

