package com.exemplo.repositorio;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.exemplo.model.Cliente;
import com.exemplo.model.Endereco;
import com.exemplo.model.Fornecedor;
import com.exemplo.model.Pedido;
import com.exemplo.model.Pessoa;
import com.exemplo.model.QCidade;
import com.exemplo.model.QEndereco;
import com.exemplo.model.QFornecedor;
import com.exemplo.model.QPedido;
import com.exemplo.model.QPessoa;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;

public class RepositorioPessoa {

	EntityManagerFactory emf;
	EntityManager em;
	JPAQuery query;

	public RepositorioPessoa() {
		emf = Persistence.createEntityManagerFactory("mohr");
		em = emf.createEntityManager();
		query = new JPAQuery(em);
	}

	public void salvar(Pessoa pessoa) {
		em.getTransaction().begin();
		em.merge(pessoa);
		em.getTransaction().commit();
		em.close();
	}

	public void salvar(Pedido pedido) {
		em.getTransaction().begin();
		em.merge(pedido);
		em.getTransaction().commit();
		em.close();
	}

	public void remover(Pessoa pessoa) {
		em.getTransaction().begin();
		em.remove(pessoa);
		em.getTransaction().commit();
		em.close();
	}

	@SuppressWarnings("unchecked")
	public List<Pessoa> listarTodos() {
		em.getTransaction().begin();
		Query consulta = em.createQuery("select pessoa from Pessoa pessoa");
		List<Pessoa> pessoas = consulta.getResultList();
		em.getTransaction().commit();
		em.close();
		return pessoas;
	}

	public static void main(String[] args) {
		RepositorioPessoa rep = new RepositorioPessoa();
		// Pessoa pessoa = new Pessoa();
		// pessoa.setDsPessoa("JOAO");
		// repPessoa.salvar(pessoa);

		// List<Pessoa> ps = repPessoa.listarTodos();
		// for (Pessoa p : ps){
		// System.out.println(p);
		// }

		// Cliente cliente = new Cliente("MARIA CLARA", "06886903942", 30);
		// cliente.addEndereco(new Endereco(cliente, "RUA ARARUNA", "159",
		// "CENTRO"));
		// cliente.addEndereco(new Endereco(cliente, "RUA PEROBAL", "220",
		// "JARDIM ARARUNA"));
		// cliente.addEndereco(new Endereco(cliente, "RUA DO MARIO", "300",
		// "JARDIM JHONIS"));
		// repPessoa.salvar(cliente);

		// Fornecedor fornecedor = new Fornecedor("MOVEIS FORTE",
		// "02656987456235", "AJ RORATO");
		// fornecedor.addEndereco(new Endereco(fornecedor, "RODOVIA 300", "",
		// "PARQUE INDISTRIAL"));
		// repPessoa.salvar(fornecedor);

		// Random gerador = new Random();
		// QFornecedor forn = QFornecedor.fornecedor;
		// List<Fornecedor> objs = rep.query.from(forn).list(forn);
		// for (Fornecedor obj : objs) {
		// rep.em.getTransaction().begin();
		// rep.em.merge(new Pedido(obj, TipoPedido.COMPRA, new BigDecimal(
		// gerador.nextInt())));
		// rep.em.getTransaction().commit();
		// }
		// rep.em.close();

		// Random gerador = new Random();
		// QCliente forn = QCliente.cliente;
		// List<Cliente> objs = rep.query.from(forn).list(forn);
		// for (Cliente obj : objs) {
		// rep.em.getTransaction().begin();
		// rep.em.merge(new Pedido(obj, TipoPedido.DEVOLUCAO, new BigDecimal(
		// gerador.nextInt())));
		// rep.em.getTransaction().commit();
		// }
		// rep.em.close();

		// rep.listarTodasPessoas();
		// rep.listarTodasPessoasClienteFornecedor();
		// rep.listarTodosFornecedores();
		// rep.listaCountPessoas();
		// rep.somaIdPessoas();
		// rep.listarTodosPedidos();
		// rep.listarQuantidadePedidosPorTipo();
		// rep.listarSomaPedidoPorTipo();
		// rep.listarQuantidadePedidoPorPessoa();
		// rep.listarSomaPedidoPorPessoa();
		// rep.listarPessoasPossuiPedidos();
		// rep.listarPessoasAtravesPedido();
		// rep.listarTodasPessoasClienteFornecedor();
		// rep.listarTodosPedidosPessoas();
		// rep.listarPessoasAtravesPedido();
		QPessoa pessoa = QPessoa.pessoa;
		List<Pessoa> pessoas = rep.query.from(pessoa).list(pessoa);
		System.out.println(pessoas);
	}

	public void listarPessoasPossuiPedidos() {
		QPessoa pes = QPessoa.pessoa;
		QPedido ped = QPedido.pedido;

		List<Pessoa> pessoas = query
				.from(pes)
				.where(new JPASubQuery().from(ped)
						.where(ped.pessoa.idPessoa.eq(pes.idPessoa)).exists())
				.list(pes);

		for (Pessoa pessoa : pessoas)
			System.out.println(pessoa);
	}

	public void listarPessoasAtravesPedido() {
		QPedido ped = QPedido.pedido;
		QPessoa pes = QPessoa.pessoa;
		QCidade cid = QCidade.cidade;
		List pessoas = query.from(ped).leftJoin(ped.pessoa, pes)
				.leftJoin(pes.cidade, cid).list(pes.enderecos, ped.id);
		for (Object p : pessoas)
			System.out.println(p);
	}

	public void listarTodasPessoas() {
		QPessoa pes = QPessoa.pessoa;
		QPedido ped = QPedido.pedido;

		Pessoa pessoa = query.from(pes).uniqueResult(pes);

		// for (Pessoa pessoa : pessoas)
		System.out.println(pessoa);
	}

	public void listarTodasPessoasClienteFornecedor() {
		QPessoa pes = QPessoa.pessoa;
		List<Tuple> pessoas = query.from(pes)
				.where(pes.instanceOfAny(Fornecedor.class, Cliente.class))
				.list(pes, pes.enderecos);

		for (Tuple pessoa : pessoas)
			System.out.println(pessoa);
	}

	public void listarTodosFornecedores() {
		QFornecedor forn = QFornecedor.fornecedor;
		List<Fornecedor> objs = query.from(forn).list(forn);
		for (Fornecedor obj : objs)
			System.out.println(obj);

		QPessoa pes = QPessoa.pessoa;
	}

	public void listaCountPessoas() {
		QPessoa pes = QPessoa.pessoa;
		long qt = query.from(pes).count();
		System.out.println("Quandidade de pessoas: " + qt);
	}

	public void somaIdPessoas() {
		QPessoa pes = QPessoa.pessoa;
		long sum = query.from(pes).singleResult(pes.idPessoa.sum());
		System.out.print("Soma dos id: " + sum);
	}

	public void listarTodosPedidos() {
		QPedido ped = QPedido.pedido;
		List<Pedido> pedidos = query.from(ped).list(ped);
		for (Pedido p : pedidos)
			System.out.println(p);
	}

	public void listarQuantidadePedidosPorTipo() {
		QPedido ped = QPedido.pedido;
		List<Tuple> pedidos = query.from(ped).groupBy(ped.tipoPedido)
				.list(ped.tipoPedido, ped.id.count());
		for (Tuple obj : pedidos) {
			System.out.println(obj.get(ped.tipoPedido) + " = "
					+ obj.get(ped.id.count()));
		}
	}

	public void listarSomaPedidoPorTipo() {
		QPedido ped = QPedido.pedido;
		List<Tuple> pedidos = query.from(ped).groupBy(ped.tipoPedido)
				.list(ped.tipoPedido, ped.vlTotal.sum());
		for (Tuple obj : pedidos) {
			System.out.println(obj.get(ped.tipoPedido) + " = "
					+ obj.get(ped.vlTotal.sum()));
		}
	}

	public void listarQuantidadePedidoPorPessoa() {
		QPedido ped = QPedido.pedido;
		QPessoa pes = QPessoa.pessoa;
		QCidade cid = QCidade.cidade;
		List<Tuple> pedidos = query.from(ped).innerJoin(ped.pessoa, pes)
				.groupBy(pes.idPessoa, pes.dsPessoa)
				.list(pes.idPessoa, pes.dsPessoa, pes.cidade, ped.id.count());
		for (Tuple obj : pedidos) {
			System.out.println("Pessoa: " + obj.get(pes.idPessoa) + "  "
					+ obj.get(pes.dsPessoa) + "  " + obj.get(pes.cidade)
					+ " , Quantidade de pedidos: " + obj.get(ped.id.count()));
		}
	}

	public void listarSomaPedidoPorPessoa() {
		QPedido ped = QPedido.pedido;
		QPessoa pes = QPessoa.pessoa;
		QCidade cid = QCidade.cidade;
		List<Tuple> pedidos = query
				.from(ped)
				.innerJoin(ped.pessoa, pes)
				.groupBy(pes.idPessoa, pes.dsPessoa)
				.list(pes.idPessoa, pes.dsPessoa, pes.cidade, ped.vlTotal.sum());
		for (Tuple obj : pedidos) {
			System.out.println("Pessoa: " + obj.get(pes.idPessoa) + "  "
					+ obj.get(pes.dsPessoa) + "  " + obj.get(pes.cidade)
					+ " , Soma de pedidos: " + obj.get(ped.vlTotal.sum()));
		}
	}

	public void listarTodosPedidosPessoas() {
		QPedido ped = QPedido.pedido;
		QPessoa pes = QPessoa.pessoa;
		List<Pedido> pedidos = query.from(ped).orderBy(ped.vlTotal.desc())
				.list(ped);
		for (Pedido pedido : pedidos) {
			System.out.println(pedido + ", Pessoa: " + pedido.getPessoa());
		}
	}

	public void ListarPedidos() {
		QPedido ped = QPedido.pedido;
		List<Pedido> pedidos = query.from(ped).list(ped);
		for (Pedido pedido : pedidos)
			System.out.println(pedido);
		
	
	}
}
