package com.exemplo.repositorio;

import java.math.BigDecimal;

import com.exemplo.model.Pessoa;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.path.StringPath;

public class TotalPedidoDTO {

	private StringPath nomePessoa;
	private NumberExpression<BigDecimal> valorTotal;

	public TotalPedidoDTO(StringPath nomePessoa,
			NumberExpression<BigDecimal> valorTotal) {
		this.nomePessoa = nomePessoa;
		this.valorTotal = valorTotal;
	}

	@Override
	public String toString() {
		return nomePessoa + " , valor " + valorTotal;
	}
}
