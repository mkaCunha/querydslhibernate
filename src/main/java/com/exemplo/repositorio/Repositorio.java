package com.exemplo.repositorio;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.exemplo.model.QCliente;
import com.mysema.query.jpa.impl.JPAQuery;

public class Repositorio {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("mohr");
		EntityManager em = emf.createEntityManager();
		JPAQuery query = new JPAQuery(em);

		QCliente qCliente = QCliente.cliente;

		query.from(qCliente).list(qCliente.enderecos.any().id, qCliente.enderecos.any().dsLogradouro);

	}
}
