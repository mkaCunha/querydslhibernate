package com.exemplo.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "CLIENTE")
public class Cliente extends Pessoa {

	@Column(name = "nr_cpf", length = 11)
	private String nrCpf;

	@Column(name = "nr_idade")
	private int nrIdade;

	public Cliente() {
	}

	public Cliente(String ds_pessoa, String nrCpf, int nrIdade) {
		super();
		super.setDsPessoa(ds_pessoa);
		this.nrCpf = nrCpf;
		this.nrIdade = nrIdade;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public int getNrIdade() {
		return nrIdade;
	}

	public void setNrIdade(int nrIdade) {
		this.nrIdade = nrIdade;
	}
}
