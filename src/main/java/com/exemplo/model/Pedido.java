package com.exemplo.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pedido")
public class Pedido implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@ManyToOne
	@JoinColumn(name = "id_pessoa_pedido", referencedColumnName = "id_pessoa", insertable = true, updatable = true)
	private Pessoa pessoa;

	@Enumerated(EnumType.STRING)
	@Column(name = "tp_pedido", length = 20)
	private TipoPedido tipoPedido;

	@Column(name = "vl_total")
	private BigDecimal vlTotal;

	@Temporal(TemporalType.DATE)
	@Column(name = "dt_pedido")
	private Date dataPedido;

	public Pedido() {
		// TODO Auto-generated constructor stub
	}

	public Pedido(Pessoa pessoa, TipoPedido tipoPedido, BigDecimal vlTotal) {
		super();
		this.pessoa = pessoa;
		this.tipoPedido = tipoPedido;
		this.vlTotal = vlTotal;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public TipoPedido getTipoPedido() {
		return tipoPedido;
	}

	public void setTipoPedido(TipoPedido tipoPedido) {
		this.tipoPedido = tipoPedido;
	}

	public BigDecimal getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}

	@Override
	public String toString() {
		return "Pedido de " + this.tipoPedido.toString() + " nr. " + this.id + ", valor do pedido: "
				+ this.vlTotal.toString();
	}
}
