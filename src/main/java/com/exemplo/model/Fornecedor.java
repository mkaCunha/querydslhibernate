package com.exemplo.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "FORNECEDOR")
public class Fornecedor extends Pessoa {

	@Column(name = "nr_cnpj", length = 14)
	private String nrCnpj;

	@Column(name = "nm_fantasia", length = 60)
	private String nmFantasia;

	public Fornecedor() {
		// TODO Auto-generated constructor stub
	}

	public Fornecedor(String ds_pessoa, String nrCnpj, String nmFantasia) {
		super();
		super.setDsPessoa(ds_pessoa);
		this.nrCnpj = nrCnpj;
		this.nmFantasia = nmFantasia;
	}

	public String getNrCnpj() {
		return nrCnpj;
	}

	public void setNrCnpj(String nrCnpj) {
		this.nrCnpj = nrCnpj;
	}

	public String getNmFantasia() {
		return nmFantasia;
	}

	public void setNmFantasia(String nmFantasia) {
		this.nmFantasia = nmFantasia;
	}
}
