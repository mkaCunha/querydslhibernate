package com.exemplo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


public class FilialPk implements Serializable{

	@Column(name = "id_filial")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idFilial;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa", insertable = true, updatable = true)
	private Empresa empresa;

	public void setIdFilial(int idFilial) {
		this.idFilial = idFilial;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public int getIdFilial() {
		return idFilial;
	}

	public Empresa getEmpresa() {
		return empresa;
	}
	
	
}
