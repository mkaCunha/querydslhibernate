package com.exemplo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "filial")
public class Filial implements Serializable {

	@EmbeddedId
	private FilialPk filialPk;

	@Column(name = "cd_filial")
	private int cdFilial;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_cidade", referencedColumnName = "id", insertable = true, updatable = true)
	private Cidade cidade;

	public FilialPk getFilialPk() {
		return filialPk;
	}

	public void setFilialPk(FilialPk filialPk) {
		this.filialPk = filialPk;
	}



	public int getCdFilial() {
		return cdFilial;
	}

	public void setCdFilial(int cdFilial) {
		this.cdFilial = cdFilial;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	

}
