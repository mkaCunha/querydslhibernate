package com.exemplo.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.print.attribute.HashAttributeSet;

@Entity
@Table(name = "pessoa")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tp_pessoa", discriminatorType = DiscriminatorType.STRING, length = 40)
public abstract class Pessoa implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pessoa")
	private int idPessoa;

	@Column(name = "ds_pessoa")
	private String dsPessoa;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pessoa", cascade = CascadeType.ALL)
	private Set<Endereco> enderecos;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pessoa", cascade = CascadeType.ALL)
	private Set<Pedido> pedidos;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_cidade", referencedColumnName = "id", insertable = true, updatable = true)
	private Cidade cidade;

	public Set<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(Set<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public int getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(int idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getDsPessoa() {
		return dsPessoa;
	}

	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}

	@Override
	public String toString() {
		return this.idPessoa + "  " + this.dsPessoa;
	}

	public void addEndereco(Endereco endereco) {
		if (this.enderecos == null)
			this.enderecos = new HashSet<Endereco>();
		this.enderecos.add(endereco);
	}

	public Set<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(Set<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public void addPedido(Pedido pedido) {
		if (this.pedidos == null)
			this.pedidos = new HashSet<Pedido>();
		this.pedidos.add(pedido);
	}
}
