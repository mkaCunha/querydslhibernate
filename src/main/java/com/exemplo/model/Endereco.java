package com.exemplo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "endereco")
public class Endereco implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@ManyToOne
	@JoinColumn(name = "id_pessoa", referencedColumnName = "id_pessoa", insertable = true, updatable = true)
	private Pessoa pessoa;

	@Column(name = "ds_logradouro", length = 60)
	private String dsLogradouro;

	@Column(name = "nr_logradouro", length = 15)
	private String nrLogradouro;

	@Column(name = "ds_bairro", length = 60)
	private String dsBairro;

	public Endereco() {
		// TODO Auto-generated constructor stub
	}

	public Endereco(Pessoa pessoa, String dsLogradouro, String nrLogradouro,
			String dsBairro) {
		super();
		this.pessoa = pessoa;
		this.dsLogradouro = dsLogradouro;
		this.nrLogradouro = nrLogradouro;
		this.dsBairro = dsBairro;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getDsLogradouro() {
		return dsLogradouro;
	}

	public void setDsLogradouro(String dsLogradouro) {
		this.dsLogradouro = dsLogradouro;
	}

	public String getNrLogradouro() {
		return nrLogradouro;
	}

	public void setNrLogradouro(String nrLogradouro) {
		this.nrLogradouro = nrLogradouro;
	}

	public String getDsBairro() {
		return dsBairro;
	}

	public void setDsBairro(String dsBairro) {
		this.dsBairro = dsBairro;
	}

	@Override
	public String toString() {
		return this.pessoa + " ENDERECO(" + this.id + ") = "
				+ this.dsLogradouro + ", " + this.nrLogradouro + ", "
				+ this.dsBairro;
	}
}
